class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  include ApplicationHelper #response for JSON
  require 'json'
  respond_to 'json'
  before_action :resp

  def resp
    @resp = Response.new
    return @resp
  end

  def sender(parameters)
    @resp.code = parameters[:code]
    @resp.data = parameters[:data]
    render :json => @resp.json
  end

end
