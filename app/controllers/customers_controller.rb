class CustomersController < UsersController
  require 'json'
  respond_to 'json'
  require 'aws/ses'
  skip_before_filter  :verify_authenticity_token
  before_action :auth_params, except: [:create, :login]
  before_action :authenticate, except: [:create, :login, :auth_params]

  def authenticate
    user = RememberToken.authenticate(RememberToken.digest(auth_params.slice[:remember_token]))
    user.instance_of?(User)? (@customer = Customer.find_by_user_id(user.id)) : (sender(error = {code: false, data: 'please login again.'}))
    @customer !=nil ? (return @customer) : (sender(error = {code: false, data: 'please login again.'}))
  end

  def create
    sender(new_customer = Customer.create_customer(customer_params, user_params , address_params))
  end

  def rent_history
    sender(rent_history = @customer.view_rent_history)
  end

  def rent_book
    sender(new_rent = @customer.employee_rent(new_rent_params))
  end

  def credit_card_info

  end

  def upgrade_category
    #pay and upgrade the status
  end


  private

  def customer_params
    return nil unless params["customer"]
    params.require(:customer).permit(:account_type, :account_status)
  end

  def address_params
    return nil unless params["address"]
    params.require(:address).permit(:street, :city, :state, :zip, :country)
  end

  def new_rent_params
    return nil unless params["rent"]
    params.require(:rent).permit(:book_id )
  end


end
