class EmployeesController < UsersController
  require 'json'
  respond_to 'json'
  skip_before_filter  :verify_authenticity_token
  before_action :auth_params, except: [:create, :login]
  before_action :authenticate, except: [:create, :login, :auth_params]
  #date format Sun, 15 Feb 2015

  def authenticate
      user = RememberToken.authenticate(RememberToken.digest(auth_params.slice[:remember_token]))
      user.instance_of?(User)? (@employee = Employee.find_by_user_id(user.id)) : (sender(error = {:code => false, :data => 'please login again.'}))
      @employee !=nil ? (return @employee) : (sender(error = {:code => false, :data => 'please login again.'}))
    end

  def create
    sender(new_employee = Employee.create_employee(employee_params, user_params, address_params))
  end


  def view_schedule
    sender(schedule = @employee.view_employee_schedule)
  end

  def make_schedule
    sender(new_schedule = @employee.create_employee_schedule(schedule_params))
  end

  def update_schedule
    sender(updated_schedule = @employee.update_employee_schedule(update_schedule_params))
  end

  def rent
    sender(new_rent = @employee.employee_rent(new_rent_params))
  end

  def book_return
    sender(returned = @employee.return(params[:id], params[:rent_id], return_params))
  end

  private

  def employee_params
    return nil unless params["employee"]
    params.require(:employee).permit(:rank, :status, :social_security, :salary)
  end

  def address_params
    return nil unless params["address"]
    params.require(:address).permit(:street, :city, :state, :zip, :country)
  end

  def new_rent_params
    return nil unless params["rent"]
    params.require(:rent).permit(:customer_id, :book_id )
  end

  def return_params
    return nil unless params["return"]
    params.require(:return).permit(:return_condition)
  end

  def schedule_params
    return nil unless params["schedule"]
    params.require(:schedule).permit!
  end

  def update_schedule_params
    return nil unless params["update"]
    params.require(:update).permit!
  end

end