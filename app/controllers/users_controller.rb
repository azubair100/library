class UsersController < ApplicationController
  require 'json'
  respond_to 'json'
  require 'aws/ses'
  skip_before_filter  :verify_authenticity_token
  before_action :auth_params, except: [:create, :login]
  before_action :authenticate, except: [:create, :login, :auth_params, :forgot_password]

  def authenticate
    @user = RememberToken.authenticate(RememberToken.digest(auth_params[:remember_token]))
    @user !=nil ? (return @user) : (sender(error = {code: false, data: 'please login again.'}))
  end

  def login
    sender(login = RememberToken.login(login_params))
  end

  def logout
    sender(logout = RememberToken.logout(RememberToken.digest(auth_params.slice[:remember_token])))
  end

  def change_phone_number
    sender(new_phone_number = @user.update_number(phone_params))
  end

  def change_password
    sender(new_password = @user.update_password(update_password_params))
  end

  def change_email
    sender(new_email = @user.update_email(email_params))
  end

  def edit_first_name
    sender(new_first_name = @user.update_first_name(first_name_params))
  end

  def edit_last_name
    sender(new_last_name = @user.update_last_name(last_name_params))
  end

  def forgot_password
    sender(resp = forgot_pass(forgot_password_params))
  end

  private

  def auth_params
    return nil unless params["auth"]
    params.require(:auth).permit(:remember_token)
  end

  def user_params
    return nil unless params["user"]
    params.require(:user).permit(:first_name, :last_name, :middle_name, :gender, :email, :phone_number,
                                     :password, :password_confirmation)
  end

  def login_params
    return nil unless params["login"]
    params.require(:login).permit(:email, :password)
  end

  def first_name_params
    return nil unless params["first_name"]
    params.require(:first_name).permit(:first_name)
  end

  def last_name_params
    return nil unless params["last_name"]
    params.require(:last_name).permit(:last_name)
  end

  def email_params
    return nil unless params["change_email"]
    params.require(:change_email).permit(:email)
  end

  def phone_params
    return nil unless params["change_number"]
    params.require(:change_number).permit(:phone_number )
  end

  def update_password_params
    return nil unless params["update_password"]
    params.require(:update_password).permit(:old_password, :password, :password_confirmation)
  end

  def forgot_password_params
    return nil unless params["forgot_password"]
    params.require(:forgot_password).permit(:email)
  end

  def address_params
    return nil unless params["address"]
    params.require(:address).permit(:street, :city, :state, :zip, :country)
  end

end
