module ApplicationHelper
  class Response
    attr_accessor :code, :description, :data, :error #allows Response.code to be accessible
    def initialize() #required to create Response.new
      @code = nil
      @description = nil
      @data = nil
      @error = Array.new
    end
    def json # Response.json returns a hash that is rendered in json.
      json_response = Hash.new
      json_response["code"] = @code
      json_response["description"] = @description
      json_response["data"] = @data ? @data : nil
      json_response["error"] = @error ? @error : false
      json_response
    end

    def has_errors(errors)
      @code = false
      @errors = errors
    end


    def invalid_credentials
      @code = false
      @errors = "Invalid Credentials"
    end
  end

end
