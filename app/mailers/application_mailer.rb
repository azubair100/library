class ApplicationMailer < ActionMailer::Base
  default from: 'yaxin_lee@aol.com'
  layout 'mailer'
end
