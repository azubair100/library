class UserMailer < ApplicationMailer

  def account_activation(user)

    @user = user
    mail( :to => @user.email,
          :subject => 'Thanks for signing up for our amazing app' )
  end

  def password_reset
    @greeting = "Hi"
    mail to: "to@example.org"
  end
end
