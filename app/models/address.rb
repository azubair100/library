class Address < ActiveRecord::Base
  belongs_to :branch, dependent: :destroy
  belongs_to :employee, dependent: :destroy
  belongs_to :customer, dependent: :destroy
  validates_presence_of :linked_id, :street, :city, :state, :zip, :country
  validates_numericality_of :zip, :in => 10000 .. 999999

  def Address.create_address(user, type, params)
    new_address = Address.new(linked_id: user, linked_type: type)
    new_address.update_attributes(params)
    new_address.save ? (return new_address) : (return nil)
  end

end
