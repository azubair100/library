class Author < ActiveRecord::Base
  has_many :author_book_relationships
  has_many :books, :through => :author_book_relationships
  validates  :first_name, :last_name, :length => {
                            minimum: 2,
                            maximum: 20,
                            too_short: 'must have at least %{count} words',
                            too_long: 'must have at most %{count} words'
                        }
end
