class AuthorBookRelationship < ActiveRecord::Base
  belongs_to :author
  belongs_to :book
  validates_presence_of :author_id, :book_name
end
