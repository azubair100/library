class Book < ActiveRecord::Base
  has_many :author_book_relationships
  has_many :authors, :through => :author_book_relationships
  validates_presence_of :isbn, :name, :pages, :rent_price, :condition

  def manage_rent_money
    #200 = 7 days,
    ratings = self.ratings
      case ratings
        when ratings <9.0
          return 5.0
        when ratings <8.0
          return 4.0
        when ratings <7.0
          return 3.0
        when ratings <6.0
          return 2.0
        when ratings ==0.0
          return 2.0
        else
          return 1.0
      end
  end

  def manage_return_day
    pages = self.pages
    case pages
      when pages < 200
        return Time.now + 7.days
      when pages < 400
        return Time.now + 10.days
      when pages < 600
        return Time.now + 12.days
      when pages <800
        return Time.now + 15.days
      when pages <1000
        return Time.now + 20.days
      when pages <1500
        return Time.now + 22.days
      when pages < 1800
        return Time.now + 25.days
      when pages < 2000
        return Time.now + 30.days
      else
        return Time.now + 2.days
    end
  end

end
