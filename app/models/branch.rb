class Branch < ActiveRecord::Base
  has_one :address
  validates_presence_of :branch_name
end
