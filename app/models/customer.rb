class Customer < ActiveRecord::Base
  require 'json'
  respond_to? 'json'
  belongs_to :user
  validates_presence_of :user_id

  def Customer.create_customer(customer, user, address)
    this_user_info = User.create_user(user, address)
     this_user_info[:code] == true ? (
      user = this_user_info[:data][:user]
      this_customer = Customer.new(user_id: user.id)
      this_customer.update_attributes(customer)
      this_customer.save
      new_address = Address.create_address(user.id, type = 'customer', address)
      if new_address.save
      return new_user = { code: true, data:{remember_token: RememberToken.sign_in(this_customer),
                                            user: user, customer: this_customer, :address => new_address}}
      else
      return new_user = { code: true, data:{remember_token: RememberToken.sign_in(this_customer),
                                            user: user, customer: this_customer, :address => 'address not created.'}}
      end)
    : ( return error = {code: false, data: this_customer.errors.full_messages})
  end

  def view_rent_history
    history = Rent.where(customer_id: self.id)
     (history != [] || !history.empty?) ? (
     return rent_history = {code: true, data: history})
    : (return rent_history = {code: false, data: 'no history'})
  end

  def rent(isbn)
    this_customer = Customer.find_by_id(self.id)
     (this_customer && this_customer.account_status != 'invalid') ? (
      book = Book.find_by_id(isbn[:book_id])
      new_rent = Rent.new(employee_id: 9999, due_date: book.manage_return_day, book_id: book.id,
                          due: book.rent_price, rent_condition: book.condition, customer_id: this_customer.id)
      new_rent.save
      return rents = {code: true, data: new_rent})
    : (return rents = {code: false, data: 'customer status invalid or customer not found'})
  end
end
