class Employee < ActiveRecord::Base
  require 'json'
  respond_to? 'json'
  belongs_to :user
  validates_presence_of :user_id, :rank, :status, :social_security, :salary

  def Employee.create_employee(employee, user, address)
    this_user_info = User.create_user(user, address)
    this_user_info[:code] == true ? (
    user = this_user_info[:data][:user]
    this_employee = Employee.new(user_id: user.id)
    this_employee.update_attributes(employee)
    this_employee.save
    new_address = Address.create_address(user.id, type = 'employee', address)
    if new_address.save
      return new_user = { code: true, data:{remember_token: RememberToken.sign_in(this_employee),
                                            user: user, employee: this_employee, :address => new_address}}
    else
      return new_user = { code: true, data:{remember_token: RememberToken.sign_in(this_employee),
                                            user: user, employee: this_employee, :address => 'address not created.'}}
    end)
    : ( return error = {code: false, data: this_employee.errors.full_messages})
  end

  def view_employee_schedule
    schedule = WorkSchedule.where(:employee_id => self.id).first
    schedule ? (return work = {code: true, data: schedule})
    : ( return work_schedule = {code: false, data: 'no schedule'})
  end

  def create_employee_schedule(credentials)
    this_employee = Employee.find_by_id(credentials[:employee_id])
    if this_employee
      if this_employee.status != 'invalid' && ( self.rank == 'owner' || self.rank == 'manager')
        new_schedule = WorkSchedule.new(credentials)
        new_schedule.update_attributes(:start_date => Date.today.beginning_of_day + 7.days,
                                       :end_date => Date.today.end_of_day + 7.days)
         new_schedule.save ? (
         return schedule = {code: true, data: new_schedule})
        : (
         return schedule = {code: false, data: new_schedule.errors.full_messages})
      else
        return schedule = {code: false, data: 'employee status invalid or you are not authorized'}
      end
    else
      return schedule = {code: false, data: 'employee not found'}
    end
  end

  def update_employee_schedule(parameters)
    this_employee = Employee.find_by_id(parameters[:id])
    if self.rank == 'owner' || self.rank == 'manager'
      if this_employee
        schedule = WorkSchedule.where(employee_id: this_employee.id).first
        schedule ? (
          schedule.update_attributes(parameters)
          schedule.save
           return update_schedule = {code: true, data: schedule})
        : (
        return update_schedule ={code: false, data: 'no schedule found'}
        )
        end
        return update_schedule ={code: false, data: 'no employee found'}
      end
      return update_schedule = {code: false, data: 'you are not authorized'}
  end

  def employee_rent(isbn)
    this_employee = Employee.find_by_id(isbn[:employee_id])
    this_employee.account_status != 'invalid'? (
        book = Book.find_by_id(isbn[:book_id])
        new_rent = Rent.new(employee_id: self.id, due_date: book.manage_return_day, book_id: book.id,
                            due: book.rent_price, rent_condition: book.condition, employee_id: this_employee.id)
        new_rent.save
        return rents = {code: true, data: new_rent})
    : (
    return rents = {code: false, data: 'employee status invalid'})
  end

  def return_book(employee, rent, return_params)
    this_employee = Employee.find_by_id(employee)
    if this_employee
      rent = Rent.find_by_id(rent)
      rent ? (
        rent.update_attributes(returned: true, return_date: Time.now,
                               fined: rent.fined_method, rent_condition: return_params[:rent_condition])
        this_book = Book.find_by_id(rent.book_id)
        this_book.update_attributes(condition: rent.rent_condition, times_rented: this_book.times_rented + 1)
        return hash = {code: true, data: {book: this_book, rent: rent}})
      : (
      return hash = {code: false, data: 'no rent found'}
      )
    else
      return hash  = {code: false, data: 'employee not in the system.'}
    end
  end

end
