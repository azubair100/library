class RememberToken < ActiveRecord::Base
  belongs_to :customer
  belongs_to :employee
  validates_uniqueness_of :token_number
  validates_presence_of :user_id, :user_type

  def RememberToken.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def RememberToken.digest(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  def RememberToken.authenticate(token)
    user = nil
    rt = RememberToken.find_by_token_number(token)
    rt != nil ? (
    user= User.find_by_id(rt.user_id)
    return user)
    : (return 'please login again.')
  end

  def RememberToken.logout(credentials)
    session = RememberToken.find_by_token_number(credentials)
    session ? (
      session.update_attributes(expired: true, token_number: 'nil')
      return logout = {code: true, data: {session: session, message: 'you have successfully logged out'}})
    : (return logout = {code: false, data: 'no such token'})
  end

  def RememberToken.login(credentials)
    this_user = User.find_by_email(credentials[:email])
    if this_user && this_user.authenticate(credentials[:password])
      e_or_c = nil
      if Employee.find_by_user_id(this_user.id) != nil
        e_or_c = Employee.find_by_user_id(this_user.id)
        return hash = {code: true, data: {remember_token: RememberToken.sign_in(e_or_c), employee: e_or_c}}
      else
        e_or_c = Customer.find_by_user_id(this_user.id)
        return hash = {code: true, data: {remember_token: RememberToken.sign_in(e_or_c), customer: e_or_c}}
      end
    else
      return hash = {code: false, data: 'please login again'}
    end

  end

  def RememberToken.sign_in(user)
    remember_token = RememberToken.new_remember_token
    new_token = RememberToken.new(user_id: user.id,
                                  token_number: RememberToken.digest(remember_token))
    user.instance_of?(Employee)? (new_token.user_type = 'employee') : (new_token.user_type = 'customer')
    new_token.save
    tokens = RememberToken.where(user_id: user.id, expired: false)
    if tokens.count > 2
      tokens.first.update_attributes(expired: true, token_number: nil)
      tokens.first.save
    end
    return remember_token
  end

end
