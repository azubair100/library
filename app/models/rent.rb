class Rent < ActiveRecord::Base
  belongs_to :customer
  validates_presence_of :employee_id, :customer_id, :book_id, :due_date, :rent_condition
  # scope :recent, where(:expired => false).order('created_at desc').limit(10)

  def create_rent(employee, customer, book)
    new_rent = Rent.new(:employee_id => employee.id, :due_date => book.manage_return_day, :book_id => book.id,
             :due => book.rent_price, :rent_condition => book.condition, :customer_id => customer.id)
    new_rent.save
    return new_rent
  end

  def fined_method
    #over due
    if Time.now > self.due_date
        return (Time.now.to_date - self.due_date.to_date).to_f
    elsif self.due_date > Time.now
        return 0.0
    else #on_time
      return 0.0
    end
  end



end


