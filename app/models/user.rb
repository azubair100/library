class User < ActiveRecord::Base
  require 'aws/ses'
  require 'json'
  respond_to? 'json'
  # languages.each {|x| puts "#{x}" }
  before_save { email.downcase! }
  has_many :remember_tokens
  has_secure_password
  validates_presence_of :gender, :phone_number
  validates  :first_name, :last_name, :middle_name, :length => {
                            minimum: 2,
                            maximum: 20,
                            too_short: 'must have at least %{count} words',
                            too_long: 'must have at most %{count} words'
                        }
  validates :password, presence: true, length: { minimum: 6, maximum: 15,
                                                 too_short: 'password too short, at least 6',
                                                 too_long: 'password too long, at max 15' }, :on => :create
  validates :email, presence: true, uniqueness: { case_sensitive: false, }, :length => {:maximum => 100}
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :update


  def User.new_temp_code
    SecureRandom.urlsafe_base64
  end

  def User.amazon_ses(user, subject, body)
    ses = AWS::SES::Base.new(
        access_key_id: '785',
        secret_access_key: 'cyt')

    ses.send_email(
        to: user.email,
        source: 'zubaircpa@gmail.com',
        subject: subject,
        text_body: body
    )
  end

  def User.create_user(user, address)
    this_user = User.new(user)
    if this_user.save

       return new_user = { code: true, data:{user: this_user}}
    else
      return error = {code: false, data: this_user.errors.full_messages }
    end
  end

  def update_number(new_number)
    self.phone_number = new_number[:phone_number]
    self.save
    return number = {code: true, data: self.phone_number}
  end

  def update_email(new_email)
    self.phone_number = new_email[:email]
    self.save
    return email = {code: true, data: self.email}
  end

  def update_first_name(name)
    self.first_name = name[:first_name]
    self.save
    return first_name = {code: true, data: self.first_name}
  end

  def update_last_name(name)
    self.last_name = name[:last_name]
    self.save
    return last_name = {code: true, data: self.last_name}
  end

  def update_password(new_password)
    self.authenticate(new_password[:old_password]) ? (
      self.password = new_password[:password]
      self.password_confirmation = new_password[:password_confirmation]
      self.save
      return password = {code: true, data:'password changed'}
     )
    : (
    return password = {code: true, data:" old password doesn't match"})
  end

  def forgot_pass(para)
    user = User.find_by_email(para[:email])
    user != nil ? (
    temp_access_token = Digest::SHA1.hexdigest(user.email+User.new_temp_code)
    user.password = temp_access_token[0 .. 12]
    user.password_confirmation = temp_access_token[0 .. 12]
    user.save
    body = "Your temporary password is #{temp_access_token}."
    User.amazon_ses(user, sub ='your temporary password', body)
    return resp ={code: true, data: 'An email has been sent with your temporary password'}
    )
    : (return resp ={code: false, data: 'No such email found.'})
  end

end
