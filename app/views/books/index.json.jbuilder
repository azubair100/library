json.array!(@books) do |book|
  json.extract! book, :id, :isbn, :name, :rent_price, :year, :publishing_house
  json.url book_url(book, format: :json)
end
