json.array!(@users) do |user|
  json.extract! user, :id, :first_name, :last_name, :middle_name, :email, :password_digest, :gender, :phone_number
  json.url user_url(user, format: :json)
end
