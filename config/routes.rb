Rails.application.routes.draw do

  resources :users
  resources :customers
  resources :employees

  #I decided to make the backend an api so that means I am sending the remember token  every time,
  # but to do a get instead of post, you can send the remember token in the header but I decided to use post

  post 'employees/create' => 'employees#create'
  post 'employees/logout' => 'employees#logout'
  post 'employees/login' => 'employees#login'
  post 'employees/view_schedule' => 'employees#view_schedule'
  post 'employees/make_schedule' => 'employees#make_schedule'
  post 'employees/rent' => 'employees#rent'
  post 'employees/change_password' => 'employees#change_password'
  post 'employees/change_phone_number' => 'employees#change_phone_number'
  post 'employees/change_email' => 'employees#change_email'
  post 'employees/edit_first_name' => 'employees#edit_first_name'
  post 'employees/edit_last_name' => 'employees#edit_last_name'
  post 'employees/forgot_password' => 'employees#edit_forgot_password'

  post 'customers/create' => 'customers#create'
  post 'customers/login' => 'customers#login'
  post 'customers/logout' => 'customers#logout'
  post 'customers/rent_history' => 'customers#rent_history'
  post 'customers/rent' => 'customers#rent'
  post 'customers/change_password' => 'customers#change_password'
  post 'customers/change_phone_number' => 'customers#change_phone_number'
  post 'customers/change_email' => 'customers#change_email'
  post 'customers/edit_first_name' => 'customers#edit_first_name'
  post 'customers/edit_last_name' => 'customers#edit_last_name'
  post 'customers/forgot_password' => 'customers#edit_forgot_password'



end


#ZXy2Bh1m-WmgLYW3khrxKQ
# The priority is based upon order of creation: first created -> highest priority.
# See how all your routes lay out with "rake routes".

# You can have the root of your site routed with "root"
# root 'welcome#index'

# Example of regular route:
#   get 'products/:id' => 'catalog#view'

# Example of named route that can be invoked with purchase_url(id: product.id)
#   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

# Example resource route (maps HTTP verbs to controller actions automatically):
#   resources :products

# Example resource route with options:
#   resources :products do
#     member do
#       get 'short'
#       post 'toggle'
#     end
#
#     collection do
#       get 'sold'
#     end
#   end

# Example resource route with sub-resources:
#   resources :products do
#     resources :comments, :sales
#     resource :seller
#   end

# Example resource route with more complex sub-resources:
#   resources :products do
#     resources :comments
#     resources :sales do
#       get 'recent', on: :collection
#     end
#   end

# Example resource route with concerns:
#   concern :toggleable do
#     post 'toggle'
#   end
#   resources :posts, concerns: :toggleable
#   resources :photos, concerns: :toggleable

# Example resource route within a namespace:
#   namespace :admin do
#     # Directs /admin/products/* to Admin::ProductsController
#     # (app/controllers/admin/products_controller.rb)
#     resources :products
#   end