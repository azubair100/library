# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#rake export:seeds_format
#rake export:seeds_format > db/seeds.rb

  books = [
      ['217F25D1636A', "piper's pit", 'fiction', 314, 0, 0.0, 0.99, 2012, 'macabre publishers', 'new', 'hardcover'],
      ['7E53A29F6E6C', 'cutting edge','thriller', 2012, 2, 7.8, 1.99, 2014, 'ledger publishing house','new','hardcover'],
      ['7E53A298C3EE', 'mizery', 'comedy', 2124, 34, 9.8, 0.99, 2015, 'leaf publishers', 'ebook'],
      ['7E5805D1636A', "piper's pot", 'fiction', 314, 0, 0.0, 0.49, 2012, 'macabre publishers', 'new', 'ebook'],
      ['862BDA8C3EAE', 'snake pit', 'horror', 4040, 35, 9.98, 2.99, 2004, 'knife publishers', 'good', 'hardcover'],
      ['FF4E10101854', 'the butcher of rostov', 'horror', 1240, 121, 8.23, 1.99, 1998, 'knife publishers', 'good', 'paperback'],
      ['57C80655CB1B', 'american history', 'history', 2542, 10, 6.3, 0.69, 1996, 'usa publishing house', 'good', 'hardcover'],
      ['8E53A298C3EE', 'mercury', 'comedy', 2124, 34, 9.8, 1.99, 2015, 'leaf publishers', 'good', 'hardcover'],
      ['81E1936233A1', 'roasting pit', 'horror', 4040, 35, 9.98, 2.99, 2004, 'knife publishers', 'good', 'ebook'],
      ['84366434C91E', 'the butcher', 'comedy', 1240, 121, 8.23, 1.99, 1998, 'knife publishers', 'good', 'hardcover'],
      ['432A7089278D', 'razors edge','thriller', 2012, 2, 7.8, 1.99, 2014, 'ledger publishing house','new','paperback'],
      ['16D5DE9F812C', 'there is no way', 'romantic', 514, 56, 8.8, 0.79, 2014, 'loxer publishers', 'good', 'paperback'],
      ['0D0D82960C55', 'the beast incarnate', 'action', 114, 56, 9.8, 2.79, 2014, 'loxer publishers', 'new', 'ebook'],
      ['6B8C70368F5C', 'baking pit', 'horror', 440, 35, 9.98, 2.99, 2004, 'knife publishers', 'good', 'paperback'],
      ['5488B3837865', 'get out', 'horror', 314, 56, 8.8, 0.79, 2014, 'loxer publishers', 'good', 'hardcover'],
      ['5A0E89880561',  'the dead man', 'action', 114, 56, 9.8, 2.89, 2015, 'loxer publishers', 'new', 'ebook']
  ]


  authors = [
      ['Lori','Burns', 'female', 'Rocky .'],
      ['Preston',	'Malone', 'male', 'Blah .'],
      ['Melanie',	'Ruiz', 'female', 'Whatever. Whatever'],
      ['Brendan',	'Larson', 'male', 'Useless, court, marx.'],
      ['Darryl',	'Walsh', 'male', 'should have could have.'],
      ['Priscilla',	'Lamb', 'female', 'rake rake rake'],
      ['Walter',	'Mcdonald', 'male', 'once again we will do something ...']
  ]

  author_book = [
      [1, "piper's pit"],
      [2, 'cutting edge'],
      [3, 'mizery'],
      [4, "piper's pot"],
      [5, 'snake pit'],
      [6, 'the butcher of rostov'],
      [7, 'american history'],
      [1, 'mercury'],
      [2, 'roasting pit'],
      [3, 'the butcher'],
      [4, 'razors edge'],
      [5, 'there is no way'],
      [6, 'the beast incarnate'],
      [7, 'baking pit'],
      [1, 'get out'],
      [2,  'the dead man']
  ]




  branches = [
      ['washington heights'],
      ['flushing']
  ]


  addresses =[
      [1, 'branch', '8945 rothster street', 'washington heights', 'ny', 11456, 'usa'],
      [2, 'branch', '3412 maxenburg street', 'flushing', 'ny', 11245, 'usa']
  ]

  books.each do |isbn, name, genre, pages, times_rented, ratings, rent_price, year, publishing_house, condition, physical_copy_type |
      Book.create( isbn: isbn, name: name, genre: genre, pages: pages, times_rented: times_rented, ratings: ratings, rent_price: rent_price, year: year, publishing_house: publishing_house, condition: condition, physical_copy_type: physical_copy_type )
    end


  authors.each do |first_name, last_name, gender, bio|
    Author.create(first_name: first_name, last_name: last_name, gender: gender, bio: bio)
  end

  author_book.each do |author_id, book_name|
    AuthorBookRelationship.create(author_id: author_id, book_name: book_name)
  end

  branches.each do |branch_name|
    Branch.create(branch_name: branch_name)
  end

  addresses.each do |linked_id, linked_name, street, city, state, zip, country|
    Address.create(linked_id: linked_id, linked_name: linked_name, street: street, city: city, state: state, zip: zip, country: country)
  end