class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.integer :linked_id, null: false
      t.string :linked_type, null: false
      t.string :street
      t.string :city
      t.string :state
      t.integer :zip
      t.string :country

      t.timestamps
    end
    add_index :addresses, :linked_id
  end
end
