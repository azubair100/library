class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :isbn, null: false
      t.string :name, null: false
      t.string :genre, null: false
      t.integer :pages, null: false
      t.integer :times_rented, default:0, null: false
      t.float :ratings, default:0.0, null: false
      t.float :rent_price, default: 0.0, null: false
      t.integer :year, default: 0, null: false
      t.string :publishing_house
      t.string :condition
      t.string :physical_copy_type

      t.timestamps
    end
  end
end
