class CreateAuthorBookRelationships < ActiveRecord::Migration
  def change
    create_table :author_book_relationships do |t|
      t.integer :author_id
      t.integer :book_id

      t.timestamps
    end
    add_index :author_book_relationships, :author_id
    add_index :author_book_relationships, :book_id
  end
end
