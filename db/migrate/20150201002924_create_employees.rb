class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.integer :user_id, null: false
      t.string :rank, null: false
      t.string :status
      t.string :social_security, limit: 10
      t.float :salary, default: 0.0, null: false

      t.timestamps
    end
    add_index :employees, :user_id
  end
end

