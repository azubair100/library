class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.integer :user_id, null: false
      t.string :account_type, default: 'basic'
      t.string :account_status, default: 'valid'

      t.timestamps
    end
    add_index :customers, :user_id
  end
end
