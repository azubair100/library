class CreateWorkSchedules < ActiveRecord::Migration
  def change
    create_table :work_schedules do |t|
      t.integer :employee_id
      t.integer :user_id
      t.date :start_date
      t.date :end_date
      t.string :monday
      t.string :tuesday
      t.string :wednesday
      t.string :thursday
      t.string :friday
      t.string :saturday
      t.string :sunday
      t.string :changes
      t.timestamps
    end
    add_index :work_schedules, :employee_id
    add_index :work_schedules, :user_id
  end
end
