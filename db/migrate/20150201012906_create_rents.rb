class CreateRents < ActiveRecord::Migration
  def change
    create_table :rents do |t|
      t.integer :customer_id
      t.integer :employee_id
      t.integer :book_id
      t.datetime :due_date
      t.datetime :returned_date
      t.float :due, default: 0.0, null: false
      t.boolean :returned, default: false
      t.float :fined, default: 0.0, null: false
      t.string :rent_condition
      t.string :return_condition

      t.timestamps
    end
    add_index :rents, :customer_id
    add_index :rents, :book_id
    add_index :rents, :employee_id
  end
end
