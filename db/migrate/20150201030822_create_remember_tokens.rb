class CreateRememberTokens < ActiveRecord::Migration
  def change
    create_table :remember_tokens do |t|
      t.integer :user_id
      t.string :user_type
      t.string :token_number
      t.boolean :expired, default: false

      t.timestamps
    end
    add_index :remember_tokens, :user_id
  end
end
