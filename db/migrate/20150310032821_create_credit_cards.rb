class CreateCreditCards < ActiveRecord::Migration
  def change
    create_table :credit_cards do |t|
      t.integer :card_number
      t.integer :customer_id
      t.string :expiration_date
      t.string :bank_name

      t.timestamps null: false
    end
    add_index :credit_cards, :customer_id
  end
end
