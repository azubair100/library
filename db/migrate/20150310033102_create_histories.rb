class CreateHistories < ActiveRecord::Migration
  def change
    create_table :histories do |t|
      t.integer :user_id
      t.string :user_type
      t.string :incidents

      t.timestamps null: false
    end
  end
end
