class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :middle_name
      t.string :email, null: false
      t.string :password_digest
      t.string :gender
      t.string :phone_number, limit: 10

      t.timestamps null: false
    end
    add_index :users, :email, unique: true
  end
end
