# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150310223448) do

  create_table "addresses", force: :cascade do |t|
    t.integer  "linked_id",   null: false
    t.string   "linked_type", null: false
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.integer  "zip"
    t.string   "country"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "addresses", ["linked_id"], name: "index_addresses_on_linked_id"

  create_table "author_book_relationships", force: :cascade do |t|
    t.integer  "author_id"
    t.integer  "book_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "author_book_relationships", ["author_id"], name: "index_author_book_relationships_on_author_id"
  add_index "author_book_relationships", ["book_id"], name: "index_author_book_relationships_on_book_id"

  create_table "authors", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "gender"
    t.string   "bio"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "books", force: :cascade do |t|
    t.string   "isbn",                             null: false
    t.string   "name",                             null: false
    t.string   "genre",                            null: false
    t.integer  "pages",                            null: false
    t.integer  "times_rented",       default: 0,   null: false
    t.float    "ratings",            default: 0.0, null: false
    t.float    "rent_price",         default: 0.0, null: false
    t.integer  "year",               default: 0,   null: false
    t.string   "publishing_house"
    t.string   "condition"
    t.string   "physical_copy_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "branches", force: :cascade do |t|
    t.string   "branch_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "branches", ["branch_name"], name: "index_branches_on_branch_name", unique: true

  create_table "credit_cards", force: :cascade do |t|
    t.integer  "card_number"
    t.integer  "customer_id"
    t.string   "expiration_date"
    t.string   "bank_name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "credit_cards", ["customer_id"], name: "index_credit_cards_on_customer_id"

  create_table "customers", force: :cascade do |t|
    t.integer  "user_id",                          null: false
    t.string   "account_type",   default: "basic"
    t.string   "account_status", default: "valid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "customers", ["user_id"], name: "index_customers_on_user_id"

  create_table "employees", force: :cascade do |t|
    t.integer  "user_id",                                  null: false
    t.string   "rank",                                     null: false
    t.string   "status"
    t.string   "social_security", limit: 10
    t.float    "salary",                     default: 0.0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "employees", ["user_id"], name: "index_employees_on_user_id"

  create_table "histories", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "incidents"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "remember_tokens", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "token_number"
    t.boolean  "expired",      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "remember_tokens", ["user_id"], name: "index_remember_tokens_on_user_id"

  create_table "rents", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "employee_id"
    t.integer  "book_id"
    t.datetime "due_date"
    t.datetime "returned_date"
    t.float    "due",              default: 0.0,   null: false
    t.boolean  "returned",         default: false
    t.float    "fined",            default: 0.0,   null: false
    t.string   "rent_condition"
    t.string   "return_condition"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rents", ["book_id"], name: "index_rents_on_book_id"
  add_index "rents", ["customer_id"], name: "index_rents_on_customer_id"
  add_index "rents", ["employee_id"], name: "index_rents_on_employee_id"

  create_table "users", force: :cascade do |t|
    t.string   "first_name",                 null: false
    t.string   "last_name",                  null: false
    t.string   "middle_name"
    t.string   "email",                      null: false
    t.string   "password_digest"
    t.string   "gender"
    t.string   "phone_number",    limit: 10
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

  create_table "work_schedules", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "user_id"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "monday"
    t.string   "tuesday"
    t.string   "wednesday"
    t.string   "thursday"
    t.string   "friday"
    t.string   "saturday"
    t.string   "sunday"
    t.string   "changes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "work_schedules", ["employee_id"], name: "index_work_schedules_on_employee_id"
  add_index "work_schedules", ["user_id"], name: "index_work_schedules_on_user_id"

end
