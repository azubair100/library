namespace :export do
  desc "Prints Address.all in a seeds.rb way."
  task :seeds_format => :environment do
    Address.order(:id).all.each do |address|
      puts "Address.create(#{address.serializable_hash.delete_if {|key, value| ['created_at','updated_at'].include?(key)}.to_s.gsub(/[{}]/,'')})"
    end
    Author.order(:id).all.each do |author|
      puts "Author.create(#{author.serializable_hash.delete_if {|key, value| ['created_at','updated_at'].include?(key)}.to_s.gsub(/[{}]/,'')})"
    end
    AuthorBookRelationship.order(:id).all.each do |authorbook|
      puts "AuthorBookRelationship.create(#{authorbook.serializable_hash.delete_if {|key, value| ['created_at','updated_at'].include?(key)}.to_s.gsub(/[{}]/,'')})"
    end
    Book.order(:id).all.each do |book|
      puts "Book.create(#{book.serializable_hash.delete_if {|key, value| ['created_at','updated_at'].include?(key)}.to_s.gsub(/[{}]/,'')})"
    end
    Branch.order(:id).all.each do |branch|
      puts "Branch.create(#{branch.serializable_hash.delete_if {|key, value| ['created_at','updated_at'].include?(key)}.to_s.gsub(/[{}]/,'')})"
    end
    CreditCard.order(:id).all.each do |card|
      puts "CreditCard.create(#{card.serializable_hash.delete_if {|key, value| ['created_at','updated_at'].include?(key)}.to_s.gsub(/[{}]/,'')})"
    end
    Customer.order(:id).all.each do |customer|
      puts "Customer.create(#{customer.serializable_hash.delete_if {|key, value| ['created_at','updated_at'].include?(key)}.to_s.gsub(/[{}]/,'')})"
    end
    Employee.order(:id).all.each do |employee|
      puts "Employee.create(#{employee.serializable_hash.delete_if {|key, value| ['created_at','updated_at'].include?(key)}.to_s.gsub(/[{}]/,'')})"
    end
    History.order(:id).all.each do |history|
      puts "History.create(#{history.serializable_hash.delete_if {|key, value| ['created_at','updated_at'].include?(key)}.to_s.gsub(/[{}]/,'')})"
    end
    RememberToken.order(:id).all.each do |token|
      puts "RememberToken.create(#{token.serializable_hash.delete_if {|key, value| ['created_at','updated_at'].include?(key)}.to_s.gsub(/[{}]/,'')})"
    end
    User.order(:id).all.each do |user|
      puts "User.create(#{user.serializable_hash.delete_if {|key, value| ['created_at','updated_at'].include?(key)}.to_s.gsub(/[{}]/,'')})"
    end
    WorkSchedule.order(:id).all.each do |schedule|
      puts "WorkSchedule.create(#{schedule.serializable_hash.delete_if {|key, value| ['created_at','updated_at'].include?(key)}.to_s.gsub(/[{}]/,'')})"
    end
  end
end