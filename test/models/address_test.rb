require 'test_helper'

class AddressTest < ActiveSupport::TestCase
 def setup
   @address = Address.new(linked_id: 1,
                          linked_type: 'branch',
                            street:'8934 123th street',
                            city: 'mac',
                            state: 'ny',
                            zip: 11244,
                            country: 'us'
   )
 end

 test 'address should be valid' do
   assert @address.valid?
 end

 test 'linked id should be present' do
   @address.linked_id = nil
   assert_not @address.valid?
 end

 test 'linked type should be present' do
   @address.linked_type = nil
   assert_not @address.valid?
 end

 test 'street should be present' do
   @address.street = nil || @address.street = ''
   assert_not @address.valid?
 end

 test 'city should be present' do
   @address.city = nil || @address.city = ''
   assert_not @address.valid?
 end

 test 'zip should be present' do
   @address.zip = nil
   assert_not @address.valid?
 end

 test 'zip should have a minimum length' do
   @address.zip = rand(0 .. 9999)
   assert @address.valid?
 end

 test 'zip should have a maximum length' do
   @address.zip = rand(1000000 .. 9999999)
   assert @address.valid?
 end


end
