require 'test_helper'

class AuthorBookRelationshipTest < ActiveSupport::TestCase
  def setup
    @author_book = AuthorBookRelationship.new(author_id: 1,
                                              book_name: 'me, myself & i'
    )
  end


  test 'author and book relationship should be valid' do
    assert @author_book.valid?
  end

  test 'author id should be present' do
    @author_book.author_id = nil
    assert_not @author_book.valid?
  end

  test 'book name should be present' do
    @author_book.book_name = ''
    assert_not @author_book.valid?
  end
end
