require 'test_helper'

class AuthorTest < ActiveSupport::TestCase
  def setup
    @author = Author.new(first_name: 'le',
                             last_name:'roof',
                             gender: 'female',
                             bio: 'basic'
    )
  end


  test 'author should be valid' do
    assert @author.valid?
  end

  test 'first name should be present' do
    @author.first_name = ' '
    assert_not @author.valid?
  end

  test 'first name should not be less than 2 letters' do
    @author.first_name = "a" * 1
    assert_not @author.valid?
  end

  test 'first name should not be more than 20 letters' do
    @author.first_name = "a" * 21
    assert_not @author.valid?
  end


  test 'last name should be present' do
    @author.last_name = ' '
    assert_not @author.valid?
  end

  test 'last name should not be less than 2 letters' do
    @author.last_name = "a" * 1
    assert_not @author.valid?
  end

  test 'last name should not be more than 20 letters' do
    @author.last_name = "a" * 21
    assert_not @author.valid?
  end


end
