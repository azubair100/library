require 'test_helper'

class BookTest < ActiveSupport::TestCase
  def setup
    @book = Book.new(isbn: '7E53A298C3EE',
                      name: 'mizery',
                      genre: 'comedy',
                      pages: 2124,
                      times_rented: 34,
                      ratings: 9.8,
                      rent_price: 0.99,
                      year: 2015,
                      condition: 'good',
                      publishing_house: 'leaf publishers',
                      physical_copy_type: 'ebook'
    )
  end


  test 'book should be valid' do
    assert @book.valid?
  end

  test 'isbn should be present' do
    @book.isbn = '' || @book.isbn = nil
    assert_not @book.valid?
  end

  test 'name should be present' do
    @book.name = '' || @book.name = nil
    assert_not @book.valid?
  end

  test 'pages should be present' do
    @book.pages = nil
    assert_not @book.valid?
  end

  test 'rent price should be present' do
    @book.rent_price = nil
    assert_not @book.valid?
  end

  test 'condition should be present' do
    @book.condition = '' || @book.condition = nil
    assert_not @book.valid?
  end

end
