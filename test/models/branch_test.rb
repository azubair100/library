require 'test_helper'

class BranchTest < ActiveSupport::TestCase
  def setup
    @branch = Branch.new(branch_name: 'woodstock')
  end

  test 'branch should be valid' do
    assert @branch.valid?
  end

  test 'branch name should be present' do
    @branch.branch_name = ' '
    assert_not @branch.valid?
  end

end