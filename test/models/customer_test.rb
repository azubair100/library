require 'test_helper'

class CustomerTest < ActiveSupport::TestCase
  def setup
    @customer = Customer.new(user_id: 3, account_status: 'valid', account_type: 'basic')
  end

  test 'employee should be valid' do
    assert @customer.valid?
  end

  test 'user id should be present' do
    @customer.user_id = nil
    assert_not @customer.valid?
  end

end