require 'test_helper'

class EmployeeTest < ActiveSupport::TestCase
  def setup
    @employee = Employee.new(user_id: 1, rank: 'manager', status: 'valid', social_security: '345657897', salary: 22.34)
  end


  test 'employee should be valid' do
    assert @employee.valid?
  end

  test 'user id should be present' do
    @employee.user_id = nil
    assert_not @employee.valid?
  end

  test 'rank should be present' do
    @employee.rank = '' || @employee.rank = nil
        assert_not @employee.valid?
  end

  test 'status should be present' do
    @employee.status = '' || @employee.status = nil
    assert_not @employee.valid?
  end

  test 'ssn should be present' do
    @employee.social_security = '' || @employee.social_security = nil
    assert_not @employee.valid?
  end

  test 'salary should be present' do
    @employee.salary = nil
    assert_not @employee.valid?
  end
  
  end




