require 'test_helper'

class RentTest < ActiveSupport::TestCase
  def setup
    @rent = Rent.new(customer_id: 1,
                     employee_id: 1,
                     book_id: 1,
                     due_date: '2015-03-04 03:51:43.363956',
                     returned_date: nil,
                     due: 1.99,
                     returned: false,
                     fined: 0.0,
                     rent_condition: 'good',
                     return_condition: nil
    )
  end


  test 'rent should be valid' do
    assert @rent.valid?
  end

  test 'customer id should be present' do
    @rent.customer_id = nil
    assert_not @rent.valid?
  end

  test 'employee id should be present' do
    @rent.employee_id = nil
    assert_not @rent.valid?
  end

  test 'book id should be present' do
    @rent.book_id = nil
    assert_not @rent.valid?
  end

  test 'due date should be present' do
    @rent.due_date = nil
    assert_not @rent.valid?
  end

  test 'rent condition should be present' do
    @rent.rent_condition = ' '
    assert_not @rent.valid?
  end

end
