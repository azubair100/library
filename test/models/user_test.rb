require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(email: 'zforzubayr@icloud.com',
                             first_name: 'ahmed',
                             last_name: 'zubair',
                             middle_name: 'mk',
                             gender: 'male',
                             phone_number: '3472453423',
                             password: '1234567',
                             password_confirmation: '1234567')
  end

  test 'user should be valid' do
    assert @user.valid?
  end

  test 'first name should be present' do
    @user.first_name = '' || @user.first_name = nil
    assert_not @user.valid?
  end

  test 'first name should not be less than 2 letters' do
    @user.first_name = "a" * 1
    assert_not @user.valid?
  end

  test 'first name should not be more than 20 letters' do
    @user.first_name = "a" * 21
    assert_not @user.valid?
  end

  test 'last name should be present' do
    @user.last_name = '' || @user.last_name = nil
    assert_not @user.valid?
  end

  test 'last name should not be less than 2 letters' do
    @user.last_name = "a" * 1
    assert_not @user.valid?
  end

  test 'last name should not be more than 20 letters' do
    @user.last_name = "a" * 21
    assert_not @user.valid?
  end

  test 'gender name should be present' do
    @user.gender = '' || @user.gender = nil
    assert_not @user.valid?
  end

  test 'phone number should be present' do
    @user.phone_number = '' || @user.phone_number = nil
    assert_not @user.valid?
  end

  test 'email should be present' do
    @user.email = ' '
    assert_not @user.valid?
  end

  test 'email should not be too long ' do
    @user.email = 'a' * 90 + '@example.com'
    assert_not @user.valid?
  end

  test 'email validation should accept valid addresses' do
    valid_addresses = %w[employee@example.com employee@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test 'email addresses should be unique' do
    duplicate_employee = @user.dup
    @user.save
    assert_not duplicate_employee.valid?
  end

  test 'email addresses should be unique two' do
    duplicate_employee = @user.dup
    duplicate_employee.email = @user.email.upcase
    @user.save
    assert_not duplicate_employee.valid?
  end

  test 'email addresses should be saved as lower-case' do
    mixed_case_email = 'Foo@ExAMPle.CoM'
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end

  test 'password should have a minimum length' do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end

  test 'password should have a maximum length' do
    @user.password = @user.password_confirmation = "a" * 16
    assert_not @user.valid?
  end




end
